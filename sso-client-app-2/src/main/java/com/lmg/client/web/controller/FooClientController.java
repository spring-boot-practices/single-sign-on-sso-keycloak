package com.lmg.client.web.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.lmg.client.web.model.FooModel;

@Controller
public class FooClientController {

    @Value("${resourceserver.api.url}")
    private String fooApiUrl;

    @GetMapping("/foos")
    public String getFoos(Model model) {
        List<FooModel> foos = Arrays.asList(
            new FooModel(1L, "foo01"),
            new FooModel(2L, "foo02")
        );
        model.addAttribute("foos", foos);
        return "foos";
    }
}
